# Multi-Domain Sentiment Analysis and Prediction

This is the source code for generating domain-specific sentiment lexicons from labeled documents. This work has been published at [**Proceedings of the 28th International Conference on Computational Linguistics, 2020**](https://aclanthology.org/volumes/2020.coling-main/).


# Literature Source
This literature can be accessed from [here](https://aclanthology.org/2020.coling-main.578/)

# Citation
If you find this work useful, please cite the following publication.

**Title:** Domain-Specific Sentiment Lexicons Induced from Labeled Documents\
**Authors:** SM Mazharul Islam, Xin Dong, Gerard de Melo\
**Abstract:** Sentiment analysis is an area of substantial relevance both in industry and in academia, including for instance in social studies. Although supervised learning algorithms have advanced considerably in recent years, in many settings it remains more practical to apply an unsupervised technique. The latter are oftentimes based on sentiment lexicons. However, existing sentiment lexicons reflect an abstract notion of polarity and do not do justice to the substantial differences of word polarities between different domains. In this work, we draw on a collection of domain-specific data to induce a set of 24 domain-specific sentiment lexicons. We rely on initial linear models to induce initial word intensity scores, and then train new deep models based on word vector representations to overcome the scarcity of the original seed data. Our analysis shows substantial differences between domains, which make domain-specific sentiment lexicons a promising form of lexical resource in downstream tasks, and the predicted lexicons indeed perform effectively on tasks such as review classification and cross-lingual word sentiment prediction.


## BibTex
@inproceedings{islam2020domain,\
&nbsp;&nbsp;&nbsp;&nbsp;title={Domain-Specific Sentiment Lexicons Induced from Labeled Documents},\
&nbsp;&nbsp;&nbsp;&nbsp;author={Islam, SM Mazharul and Dong, Xin Luna and de Melo, Gerard},\
&nbsp;&nbsp;&nbsp;&nbsp;booktitle={Proceedings of the 28th International Conference on Computational Linguistics},\
&nbsp;&nbsp;&nbsp;&nbsp;pages={6576--6587},\
&nbsp;&nbsp;&nbsp;&nbsp;year={2020}\
}


## Instructions
Major experiment parameters can be adjusted in hparams.py


## Cautions
Setting **WRITE_CSV** as **True** will over-ride previously written CSV file. So, keeping a backup of these CSV files might be a good idea. \
Setting **LOAD_PRE_TRAINED** as **False** will train the predictor from the start and over-ride the previously saved model. So, keeping a backup of these pretrained models might be a good idea.


## Experiments
In total, we have run eight different experiments.

### Prediction on VADER Lexicon
We train/test a simple predictor model on VADER lexicon

### Prediction on AM Lexicon
We train/test the same predictor model over 24 different domains

### Extract tokens with extreme sentiments
Here we extract the top 20 positive and negative sentiment tokens from each domain from AM lexicon

### Prediction on GloVe Vocabulary
We train domain-specific predictors on AM lexicon, then predict sentiments on GloVe vocabularies

### Non-Zero/Relevant token counter
We count the number of tokens whose absolute sentiment is over some threshold for each domain

### Correlation between VADER, AM and GloVe
We fix VADER as our groud truth, then compute the sentiment correlation between VADER & AM and VADER & GloVe

### Document Sentiments
Using the IMDB-movie review dataset, the sentiment of each review has been predicted. The reviews are classified as either positive and negative. So, we predicted sentiment of each word in the review and then took the average or summed them all to predict the overall sentiment.

### Multi-lingual Sentiment Prediction
We have trained the model on the English aligned vector and then predicted sentiments from other languages using the corresponding aligned vectors.


## Data

Several data sources have been used to run experiments.

- VADER lexicon. [\[link\]](https://github.com/cjhutto/vaderSentiment)
- Amazon multi-domain sentiment dataset.
- Aligned vectors from fastText. [\[link\]](https://fasttext.cc/docs/en/aligned-vectors.html)
- Word embeddings from fastText.
- Word embeddings from GloVe.
- IMDB movie review dataset.
