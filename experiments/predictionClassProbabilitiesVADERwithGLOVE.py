# EXPERIMENT: PREDICTION ON VADER LEXICON WITH GLOVE EMBEDDINGS
import os.path
import pickle

import numpy as np
import pandas as pd
from sklearn.utils import shuffle
from keras.callbacks import ModelCheckpoint, EarlyStopping, ReduceLROnPlateau
from keras.optimizers import Adam
from keras.models import load_model

from helpers.helpers import filter_with_embedding, split_dataset
from models.hparams import *
from models.models import sentiment_class_predictor, SentimentClassGenerator

# LOAD GLOVE EMBEDDING
glove_path = '../data/glove.840B.300d.pkl'
with open(glove_path, 'rb') as f:
    glove_embeddings = pickle.load(f)

# LOAD VADER LEXICON [TRAIN & TEST DATA]
vader_path = '../data/vader_lexicon.txt'
columns_names = ['token', 'mean_score', 'std', 'raw_score']
vader_lexi = pd.read_csv(vader_path, sep='\t', names=columns_names)
x = vader_lexi['token']
y = vader_lexi['mean_score']

x, y = filter_with_embedding(x, y, glove_embeddings)
x, y = shuffle(x, y, random_state=RAND_SEED)

min_score = np.min(y)
max_score = np.max(y)
n_classes = 11
class_step = (max_score - min_score)/(n_classes-2)
class_medians = np.arange(min_score-0.5*class_step, max_score+class_step, class_step)

portion_test = 0.2
portion_valid = 0.2
polarity_threshold = 0.25
train_x, valid_x, train_y, valid_y, test_x, test_y = split_dataset(x, y, portion_test, portion_valid,
                                                                   polarity_threshold)

# DEFINE MODEL
model_path = '../pretrained/VADER/Vader_ClassPredictor_with_GLOVE.h5'
checkpoint = ModelCheckpoint(filepath=model_path, monitor='val_loss', mode='min', verbose=1,
                             save_best_only=True, save_weights_only=False)
reduceLROnPlat = ReduceLROnPlateau(monitor='val_loss', mode='min', verbose=1, factor=0.5, min_delta=0.001, patience=4)
earlyStop = EarlyStopping(monitor='val_loss', mode='min', patience=20)

callback_list = [checkpoint, reduceLROnPlat, earlyStop]

train_generator = SentimentClassGenerator(train_x, train_y, n_classes, class_medians, class_step, glove_embeddings,
                                          BATCH_SIZE, is_train=True)
valid_generator = SentimentClassGenerator(valid_x, valid_y, n_classes, class_medians, class_step, glove_embeddings,
                                          BATCH_SIZE, is_train=False)

# LOAD/COMPILE MODEL
if MODEL_MODE == 'USE_PRE_TRAINED' and os.path.isfile(model_path):
    model = load_model(model_path)

elif MODEL_MODE == 'RESUME_TRAINING' and os.path.isfile(model_path):
    model = load_model(model_path)

    # TRAIN
    model.fit_generator(
        train_generator,
        steps_per_epoch=np.ceil(float(len(train_x)) / float(BATCH_SIZE)),
        validation_data=valid_generator,
        validation_steps=np.ceil(float(len(valid_x)) / float(BATCH_SIZE)),
        epochs=N_EPOCHS,
        verbose=0,
        callbacks=callback_list
    )

elif MODEL_MODE == 'RESET_MODEL' or not os.path.isfile(model_path):
    model_name = 'VADER_classPredictor'
    model = sentiment_class_predictor(model_name, n_classes, input_shape=(300,))
    model.compile(optimizer=Adam(lr=1e-3), loss='mse', metrics=['acc'])

    # TRAIN
    model.fit_generator(
        train_generator,
        steps_per_epoch=np.ceil(float(len(train_x)) / float(BATCH_SIZE)),
        validation_data=valid_generator,
        validation_steps=np.ceil(float(len(valid_x)) / float(BATCH_SIZE)),
        epochs=N_EPOCHS,
        verbose=0,
        callbacks=callback_list
    )

else:
    raise NameError('Un-specified mode for model!')

# PREDICTIONS
features = [glove_embeddings[token] for token in test_x]
sentiment_class_probabilities = np.reshape(model.predict(np.array(features)), (len(test_x), n_classes))
max_probability_class = np.argmax(sentiment_class_probabilities, 1)

sentiment_tokens = {'Tokens': test_x.values}
sentiment_df = pd.DataFrame(sentiment_tokens)

gt_truths = list()
predictions = list()
predicted_scores = list()
normalized_predicted_scores = list()

for index, token in enumerate(test_x):
    closest_index = np.argmin(np.abs(class_medians - test_y.values[index]))
    closest_probability = 1 - np.abs(test_y.values[index] - class_medians[closest_index]) / class_step

    if test_y.values[index] < class_medians[closest_index]:
        next_closest_index = closest_index - 1
        next_closest_probability = 1 - closest_probability
    else:
        next_closest_index = closest_index + 1
        next_closest_probability = 1 - closest_probability

    gt_truth = 'Class {} with {:.3f} and Class {} with {:.3f}'.format(closest_index, closest_probability,
                                                                      next_closest_index, next_closest_probability)
    gt_truths.append(gt_truth)

    if sentiment_class_probabilities[index, max_probability_class[index]-1] > \
            sentiment_class_probabilities[index, max_probability_class[index]+1]:
        prediction = 'Class {} with {:.3f} and Class {} with {:.3f}'.\
            format(max_probability_class[index], sentiment_class_probabilities[index, max_probability_class[index]],
                   max_probability_class[index]-1, sentiment_class_probabilities[index, max_probability_class[index]-1])
        predicted_score = sentiment_class_probabilities[index, max_probability_class[index]] * class_medians[max_probability_class[index]] + \
                          sentiment_class_probabilities[index, max_probability_class[index]-1] * class_medians[max_probability_class[index]-1]
        normalized_predicted_score = \
            predicted_score / (sentiment_class_probabilities[index, max_probability_class[index]] +
                               sentiment_class_probabilities[index, max_probability_class[index]-1])
    else:
        prediction = 'Class {} with {:.3f} and Class {} with {:.3f}'.\
            format(max_probability_class[index], sentiment_class_probabilities[index, max_probability_class[index]],
                   max_probability_class[index]+1, sentiment_class_probabilities[index, max_probability_class[index]+1])
        predicted_score = sentiment_class_probabilities[index, max_probability_class[index]] * class_medians[max_probability_class[index]] + \
                          sentiment_class_probabilities[index, max_probability_class[index]+1] * class_medians[max_probability_class[index]+1]
        normalized_predicted_score = \
            predicted_score / (sentiment_class_probabilities[index, max_probability_class[index]] +
                               sentiment_class_probabilities[index, max_probability_class[index]+1])

    predictions.append(prediction)
    predicted_scores.append(predicted_score)
    normalized_predicted_scores.append(normalized_predicted_score)

sentiment_df['Sentiment Score'] = test_y.values
sentiment_df['Ground Truth'] = gt_truths
sentiment_df['Prediction'] = predictions
sentiment_df['Predicted Score'] = predicted_scores
sentiment_df['Normalized Predicted Score'] = normalized_predicted_scores


# SAVE PREDICTIONS
if WRITE_CSV:
    print('\nWriting into CSV')
    sentiment_df.to_csv('../outputs/predicted_VADERsentimentClasses_glove.csv')


# PEARSON CORRELATIONS
if COMMAND_LINE:
    prediction_pearsonCoef = np.corrcoef(test_y.values, predicted_scores)
    print('Pearson Coefficient for test split with raw score: %f' % (prediction_pearsonCoef[0, 1]))

    norm_prediction_pearsonCoef = np.corrcoef(test_y.values, normalized_predicted_scores)
    print('Pearson Coefficient for test split with normalized score: %f' % (norm_prediction_pearsonCoef[0, 1]))
