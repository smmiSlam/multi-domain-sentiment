# EXPERIMENT: DOCUMENT SENTIMENT PREDICTION
import os.path
import pickle
import re
from os import listdir

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from tqdm import tqdm
from models.hparams import *


# LIST OF DOMAINS
domain_path = '../data/multi-domain-am/domain_list'
domain_list = []
with open(domain_path, 'r') as f:
    for line in f:
        domain_list.append(line[:-1])
    domain_list[-1] = line
domain_list = domain_list[1:]

# SET-UP OUTPUT DATAFRAME
df_output = pd.DataFrame({'Domains': domain_list})

# DOCUMENT PATHS
review_dirs = ['../data/aclImdb/test/pos', '../data/aclImdb/test/neg']
review_polarity = [1, -1]


# PREDICTING DOCUMENT SENTIMENTS FROM VADER BASED LEXICON [DOMAIN FREE]

# LOAD RAW VADER LEXICON
vader_path = '../data/vader_lexicon.txt'
columns_names = ['token', 'mean_score', 'std', 'raw_score']
vader_lexi = pd.read_csv(vader_path, sep='\t', names=columns_names)
# INITIALIZE LISTS TO STORE PREDICTION ACCURACY
list_accuracy_cum_prediction_vader = list()
list_threshold_cum_polarity_vader = list()
list_acccuray_avg_prediction_vader = list()
list_threshold_avg_polarity_vader = list()
# ONLY ONE DOMAIN HERE
sentiment_vader = vader_lexi['mean_score']
sentiment_vader.index = vader_lexi['token']
dict_sentiment_vader = sentiment_vader.to_dict()

# LOAD VADER-GLOVE LEXICON
lexicon_path = '../outputs/predicted_sentiments_glove_from_VADER.csv'
lexicon = pd.read_csv(lexicon_path, index_col=False)
# INITIALIZE LISTS TO STORE PREDICTION ACCURACY
list_accuracy_cum_prediction_vader_glove = list()
list_threshold_cum_polarity_vader_glove = list()
list_acccuray_avg_prediction_vader_glove = list()
list_threshold_avg_polarity_vader_glove = list()
# ONLY ONE DOMAIN HERE
sentiment_vader_glove = lexicon['Scores']
sentiment_vader_glove.index = lexicon['Tokens']
dict_sentiment_vader_glove = sentiment_vader_glove.to_dict()

# LOAD VADER-CRAWL LEXICON
lexicon_path = '../outputs/predicted_sentiments_crawl_from_VADER.csv'
lexicon = pd.read_csv(lexicon_path, index_col=False)
# INITIALIZE LISTS TO STORE PREDICTION ACCURACY
list_accuracy_cum_prediction_vader_crawl = list()
list_threshold_cum_polarity_vader_crawl = list()
list_acccuray_avg_prediction_vader_crawl = list()
list_threshold_avg_polarity_vader_crawl = list()
# ONLY ONE DOMAIN HERE
sentiment_vader_crawl = lexicon['Scores']
sentiment_vader_crawl.index = lexicon['Tokens']
dict_sentiment_vader_crawl = sentiment_vader_crawl.to_dict()

# INITIALIZE LISTS
list_true_polarity = list()
list_cumulative_sentiment_score_vader = list()
list_average_sentiment_score_vader = list()
list_cumulative_sentiment_score_vader_glove = list()
list_average_sentiment_score_vader_glove = list()
list_cumulative_sentiment_score_vader_crawl = list()
list_average_sentiment_score_vader_crawl = list()
# INITIALIZE COUNTER FOR COUNTING DOCUMENTS
n_documents = 0

# LOAD REVIEW DOCUMENTS ONE BY ONE
for current_dir, current_polarity in zip(review_dirs, review_polarity):
    file_names = sorted(listdir(current_dir))
    for file_name in file_names:
        current_file_path = os.path.join(current_dir, file_name)
        with open(current_file_path, 'r') as f:
            n_documents += 1
            list_true_polarity.append(current_polarity)
            content_tokens = f.read().replace(',', '').replace('.', '').split()
            # PREDICT SENTIMENT WITH RAW VADER
            wordwise_sentiment_scores_vader = [dict_sentiment_vader[token] for token in content_tokens
                                               if token in dict_sentiment_vader.keys()]
            # AS VADER-LEXICON IS VERY SMALL, FORCING ZERO SCORE IN CASE NO MATCHED TOKENS WERE FOUND
            if len(wordwise_sentiment_scores_vader) == 0:
                wordwise_sentiment_scores_vader = [0]
            cumulative_sentiment_score_vader = np.sum(wordwise_sentiment_scores_vader)
            average_sentiment_score_vader = np.average(wordwise_sentiment_scores_vader)
            list_cumulative_sentiment_score_vader.append(cumulative_sentiment_score_vader)
            list_average_sentiment_score_vader.append(average_sentiment_score_vader)

            # PREDICT SENTIMENT WITH VADER-GLOVE
            wordwise_sentiment_scores_vader_glove = [dict_sentiment_vader_glove[token] for token in content_tokens
                                                     if token in dict_sentiment_vader_glove.keys()]
            cumulative_sentiment_score_vader_glove = np.sum(wordwise_sentiment_scores_vader_glove)
            average_sentiment_score_vader_glove = np.average(wordwise_sentiment_scores_vader_glove)
            list_cumulative_sentiment_score_vader_glove.append(cumulative_sentiment_score_vader_glove)
            list_average_sentiment_score_vader_glove.append(average_sentiment_score_vader_glove)
            # PREDICT SENTIMENT WITH VADER-CRAWL
            wordwise_sentiment_scores_vader_crawl = [dict_sentiment_vader_crawl[token] for token in content_tokens
                                                     if token in dict_sentiment_vader_crawl.keys()]
            cumulative_sentiment_score_vader_crawl = np.sum(wordwise_sentiment_scores_vader_crawl)
            average_sentiment_score_vader_crawl = np.average(wordwise_sentiment_scores_vader_crawl)
            list_cumulative_sentiment_score_vader_crawl.append(cumulative_sentiment_score_vader_crawl)
            list_average_sentiment_score_vader_crawl.append(average_sentiment_score_vader_crawl)

# FOR VADER
# CALCULATE SENTIMENT SCORE AND POLARITY, BY SUMMING ALL TOKEN-WISE SENTIMENTS AND DISCARDING NON-FOUND REVIEW-TOKENS
predicted_cum_polarity_vader = -1 * np.ones((len(list_cumulative_sentiment_score_vader), ), dtype=np.int8)
threshold_cum_polarity_vader = \
    np.sum(list_cumulative_sentiment_score_vader)/(n_documents - np.sum(np.array(list_cumulative_sentiment_score_vader) == 0))
predicted_cum_polarity_vader[np.array(list_cumulative_sentiment_score_vader) >= threshold_cum_polarity_vader] = 1
# CALCULATE SENTIMENT SCORE AND POLARITY, BY AVERAGING ALL TOKEN-WISE SENTIMENTS
predicted_avg_polarity_vader = -1 * np.ones((len(list_average_sentiment_score_vader), ), dtype=np.int8)
threshold_avg_polarity_vader = \
    np.sum(list_average_sentiment_score_vader)/(n_documents - np.sum(np.array(list_average_sentiment_score_vader) == 0))
predicted_avg_polarity_vader[np.array(list_average_sentiment_score_vader) >= threshold_avg_polarity_vader] = 1
# CALCULATE SENTIMENT POLARITY PREDICTION ACCURACY
n_correct_cum_prediction = np.sum((np.array(list_true_polarity) * predicted_cum_polarity_vader) > 0) \
                           - np.sum(np.array(list_cumulative_sentiment_score_vader) == 0)
cum_accuracy_vader = n_correct_cum_prediction/n_documents
n_correct_avg_prediction = np.sum((np.array(list_true_polarity) * predicted_avg_polarity_vader) > 0) \
                           - np.sum(np.array(list_average_sentiment_score_vader) == 0)
avg_accuracy_vader = n_correct_avg_prediction/n_documents

# FOR VADER-GLOVE
# CALCULATE SENTIMENT SCORE AND POLARITY, BY SUMMING ALL TOKEN-WISE SENTIMENTS
predicted_cum_polarity_vader_glove = -1 * np.ones((len(list_cumulative_sentiment_score_vader_glove), ), dtype=np.int8)
threshold_cum_polarity_vader_glove = np.average(list_cumulative_sentiment_score_vader_glove)
predicted_cum_polarity_vader_glove[np.array(list_cumulative_sentiment_score_vader_glove) >= threshold_cum_polarity_vader_glove] = 1
# CALCULATE SENTIMENT SCORE AND POLARITY, BY AVERAGING ALL TOKEN-WISE SENTIMENTS
predicted_avg_polarity_vader_glove = -1 * np.ones((len(list_average_sentiment_score_vader_glove), ), dtype=np.int8)
threshold_avg_polarity_vader_glove = np.average(list_average_sentiment_score_vader_glove)
predicted_avg_polarity_vader_glove[np.array(list_average_sentiment_score_vader_glove) >= threshold_avg_polarity_vader_glove] = 1
# CALCULATE SENTIMENT POLARITY PREDICTION ACCURACY
n_correct_cum_prediction = np.sum((np.array(list_true_polarity) * predicted_cum_polarity_vader_glove) > 0)
cum_accuracy_vader_glove = n_correct_cum_prediction/n_documents
n_correct_avg_prediction = np.sum((np.array(list_true_polarity) * predicted_avg_polarity_vader_glove) > 0)
avg_accuracy_vader_glove = n_correct_avg_prediction/n_documents

# FOR VADER-CRAWL
# CALCULATE SENTIMENT SCORE AND POLARITY, BY SUMMING ALL TOKEN-WISE SENTIMENTS
predicted_cum_polarity_vader_crawl = -1 * np.ones((len(list_cumulative_sentiment_score_vader_crawl), ), dtype=np.int8)
threshold_cum_polarity_vader_crawl = np.average(list_cumulative_sentiment_score_vader_crawl)
predicted_cum_polarity_vader_crawl[np.array(list_cumulative_sentiment_score_vader_crawl) >= threshold_cum_polarity_vader_crawl] = 1
# CALCULATE SENTIMENT SCORE AND POLARITY, BY AVERAGING ALL TOKEN-WISE SENTIMENTS
predicted_avg_polarity_vader_crawl = -1 * np.ones((len(list_average_sentiment_score_vader_crawl), ), dtype=np.int8)
threshold_avg_polarity_vader_crawl = np.average(list_average_sentiment_score_vader_crawl)
predicted_avg_polarity_vader_crawl[np.array(list_average_sentiment_score_vader_crawl) >= threshold_avg_polarity_vader_crawl] = 1
# CALCULATE SENTIMENT POLARITY PREDICTION ACCURACY
n_correct_cum_prediction = np.sum((np.array(list_true_polarity) * predicted_cum_polarity_vader_crawl) > 0)
cum_accuracy_vader_crawl = n_correct_cum_prediction/n_documents
n_correct_avg_prediction = np.sum((np.array(list_true_polarity) * predicted_avg_polarity_vader_crawl) > 0)
avg_accuracy_vader_crawl = n_correct_avg_prediction/n_documents

# STORE PERFORMANCE FOR ALL DOMAIN [IDENTICAL AS THESE ARE DOMAIN-FREE]
list_threshold_cum_polarity_vader = [threshold_cum_polarity_vader for _ in range(24)]
list_accuracy_cum_prediction_vader = [cum_accuracy_vader for _ in range(24)]
list_threshold_avg_polarity_vader = [threshold_avg_polarity_vader for _ in range(24)]
list_acccuray_avg_prediction_vader = [avg_accuracy_vader for _ in range(24)]
list_threshold_cum_polarity_vader_glove = [threshold_cum_polarity_vader_glove for _ in range(24)]
list_accuracy_cum_prediction_vader_glove = [cum_accuracy_vader_glove for _ in range(24)]
list_threshold_avg_polarity_vader_glove = [threshold_avg_polarity_vader_glove for _ in range(24)]
list_acccuray_avg_prediction_vader_glove = [avg_accuracy_vader_glove for _ in range(24)]
list_threshold_cum_polarity_vader_crawl = [threshold_cum_polarity_vader_crawl for _ in range(24)]
list_accuracy_cum_prediction_vader_crawl = [cum_accuracy_vader_crawl for _ in range(24)]
list_threshold_avg_polarity_vader_crawl = [threshold_avg_polarity_vader_crawl for _ in range(24)]
list_acccuray_avg_prediction_vader_crawl = [avg_accuracy_vader_crawl for _ in range(24)]

# SAVE ACCURACY ON VADER BASED LEXICON INTO THE OUTPUT DATAFRAME
# df_output['Threshold for Cumulative, vader'] = list_threshold_cum_polarity_vader
df_output['Accuracy for Cumulative, vader'] = list_accuracy_cum_prediction_vader
# df_output['Threshold for Average, vader'] = list_threshold_avg_polarity_vader
df_output['Accuracy for Average, vader'] = list_acccuray_avg_prediction_vader
# df_output['Threshold for Cumulative, vader_glove'] = list_threshold_cum_polarity_vader_glove
df_output['Accuracy for Cumulative, vader_glove'] = list_accuracy_cum_prediction_vader_glove
# df_output['Threshold for Average, vader_glove'] = list_threshold_avg_polarity_vader_glove
df_output['Accuracy for Average, vader_glove'] = list_acccuray_avg_prediction_vader_glove
# df_output['Threshold for Cumulative, vader_crawl'] = list_threshold_cum_polarity_vader_crawl
df_output['Accuracy for Cumulative, vader_crawl'] = list_accuracy_cum_prediction_vader_crawl
# df_output['Threshold for Average, vader_crawl'] = list_threshold_avg_polarity_vader_crawl
df_output['Accuracy for Average, vader_crawl'] = list_acccuray_avg_prediction_vader_crawl


# PREDICTING DOCUMENT SENTIMENTS FROM AM BASED LEXICON [DOMAIN-SPECIFIC]

# LOAD THE AM-GLOVE LEXICON
lexicon_path = '../outputs/predicted_sentiments_glove.csv'
lexicon_am_glove = pd.read_csv(lexicon_path, index_col=False)
# INITIALIZE LISTS TO STORE PREDICTION ACCURACY
list_accuracy_cum_prediction_am_glove = list()
list_threshold_cum_polarity_am_glove = list()
list_acccuray_avg_prediction_am_glove = list()
list_threshold_avg_polarity_am_glove = list()

# LOAD AM-CRAWL LEXICON
lexicon_path = '../outputs/predicted_sentiments_crawl.csv'
lexicon_am_crawl = pd.read_csv(lexicon_path, index_col=False)
# INITIALIZE LISTS TO STORE PREDICTION ACCURACY
list_accuracy_cum_prediction_am_crawl = list()
list_threshold_cum_polarity_am_crawl = list()
list_acccuray_avg_prediction_am_crawl = list()
list_threshold_avg_polarity_am_crawl = list()

# ITERATE THROUGH DOMAINS
for domain_index in tqdm(range(24)):
    domain_sentiment = lexicon_am_glove[domain_list[domain_index]]
    domain_sentiment.index = lexicon_am_glove['Tokens']
    dict_sentiment_am_glove = domain_sentiment.to_dict()

    domain_sentiment = lexicon_am_crawl[domain_list[domain_index]]
    domain_sentiment.index = lexicon_am_crawl['Tokens']
    dict_sentiment_am_crawl = domain_sentiment.to_dict()

    # INITIALIZE LISTS
    list_true_polarity = list()
    list_cumulative_sentiment_score_am_glove = list()
    list_average_sentiment_score_am_glove = list()
    list_cumulative_sentiment_score_am_crawl = list()
    list_average_sentiment_score_am_crawl = list()
    # INITIALIZE COUNTER FOR COUNTING DOCUMENTS
    n_documents = 0

    # LOAD REVIEW DOCUMENTS ONE BY ONE
    for current_dir, current_polarity in zip(review_dirs, review_polarity):
        file_names = sorted(listdir(current_dir))
        for file_name in file_names:
            current_file_path = os.path.join(current_dir, file_name)
            with open(current_file_path, 'r') as f:
                n_documents += 1
                list_true_polarity.append(current_polarity)
                content_tokens = f.read().replace(',', '').replace('.', '').split()
                # PREDICT SENTIMENT WITH AM-GLOVE
                wordwise_sentiment_scores_am_glove = [dict_sentiment_am_glove[token] for token in content_tokens
                                                         if token in dict_sentiment_am_glove.keys()]
                cumulative_sentiment_score_am_glove = np.sum(wordwise_sentiment_scores_am_glove)
                average_sentiment_score_am_glove = np.average(wordwise_sentiment_scores_am_glove)
                list_cumulative_sentiment_score_am_glove.append(cumulative_sentiment_score_am_glove)
                list_average_sentiment_score_am_glove.append(average_sentiment_score_am_glove)
                # PREDICT SENTIMENT WITH AM-CRAWL
                wordwise_sentiment_scores_am_crawl = [dict_sentiment_am_crawl[token] for token in content_tokens
                                                         if token in dict_sentiment_am_crawl.keys()]
                cumulative_sentiment_score_am_crawl = np.sum(wordwise_sentiment_scores_am_crawl)
                average_sentiment_score_am_crawl = np.average(wordwise_sentiment_scores_am_crawl)
                list_cumulative_sentiment_score_am_crawl.append(cumulative_sentiment_score_am_crawl)
                list_average_sentiment_score_am_crawl.append(average_sentiment_score_am_crawl)

    # FOR AM-GLOVE
    # CALCULATE SENTIMENT SCORE AND POLARITY, BY SUMMING ALL TOKEN-WISE SENTIMENTS
    predicted_cum_polarity_am_glove = -1 * np.ones((len(list_cumulative_sentiment_score_am_glove), ), dtype=np.int8)
    threshold_cum_polarity_am_glove = np.average(list_cumulative_sentiment_score_am_glove)
    predicted_cum_polarity_am_glove[
        np.array(list_cumulative_sentiment_score_am_glove) >= threshold_cum_polarity_am_glove] = 1
    # CALCULATE SENTIMENT SCORE AND POLARITY, BY AVERAGING ALL TOKEN-WISE SENTIMENTS
    predicted_avg_polarity_am_glove = -1 * np.ones((len(list_average_sentiment_score_am_glove), ), dtype=np.int8)
    threshold_avg_polarity_am_glove = np.average(list_average_sentiment_score_am_glove)
    predicted_avg_polarity_am_glove[
        np.array(list_average_sentiment_score_am_glove) >= threshold_avg_polarity_am_glove] = 1
    # CALCULATE SENTIMENT POLARITY PREDICTION ACCURACY
    n_correct_cum_prediction = np.sum((np.array(list_true_polarity) * predicted_cum_polarity_am_glove) > 0)
    cum_accuracy_am_glove = n_correct_cum_prediction / n_documents
    n_correct_avg_prediction = np.sum((np.array(list_true_polarity) * predicted_avg_polarity_am_glove) > 0)
    avg_accuracy_am_glove = n_correct_avg_prediction / n_documents

    # FOR VADER-CRAWL
    # CALCULATE SENTIMENT SCORE AND POLARITY, BY SUMMING ALL TOKEN-WISE SENTIMENTS
    predicted_cum_polarity_am_crawl = -1 * np.ones((len(list_cumulative_sentiment_score_am_crawl), ), dtype=np.int8)
    threshold_cum_polarity_am_crawl = np.average(list_cumulative_sentiment_score_am_crawl)
    predicted_cum_polarity_am_crawl[
        np.array(list_cumulative_sentiment_score_am_crawl) >= threshold_cum_polarity_am_crawl] = 1
    # CALCULATE SENTIMENT SCORE AND POLARITY, BY AVERAGING ALL TOKEN-WISE SENTIMENTS
    predicted_avg_polarity_am_crawl = -1 * np.ones((len(list_average_sentiment_score_am_crawl), ), dtype=np.int8)
    threshold_avg_polarity_am_crawl = np.average(list_average_sentiment_score_am_crawl)
    predicted_avg_polarity_am_crawl[
        np.array(list_average_sentiment_score_am_crawl) >= threshold_avg_polarity_am_crawl] = 1
    # CALCULATE SENTIMENT POLARITY PREDICTION ACCURACY
    n_correct_cum_prediction = np.sum((np.array(list_true_polarity) * predicted_cum_polarity_am_crawl) > 0)
    cum_accuracy_am_crawl = n_correct_cum_prediction / n_documents
    n_correct_avg_prediction = np.sum((np.array(list_true_polarity) * predicted_avg_polarity_am_crawl) > 0)
    avg_accuracy_am_crawl = n_correct_avg_prediction / n_documents

    # STORE PERFORMANCE FOR CURRENT DOMAIN
    list_threshold_cum_polarity_am_glove.append(threshold_cum_polarity_am_glove)
    list_accuracy_cum_prediction_am_glove.append(cum_accuracy_am_glove)
    list_threshold_avg_polarity_am_glove.append(threshold_avg_polarity_am_glove)
    list_acccuray_avg_prediction_am_glove.append(avg_accuracy_am_glove)
    list_threshold_cum_polarity_am_crawl.append(threshold_cum_polarity_am_crawl)
    list_accuracy_cum_prediction_am_crawl.append(cum_accuracy_am_crawl)
    list_threshold_avg_polarity_am_crawl.append(threshold_avg_polarity_am_crawl)
    list_acccuray_avg_prediction_am_crawl.append(avg_accuracy_am_crawl)

# SAVE ACCURACY ON AM BASED LEXICON INTO THE OUTPUT DATAFRAME
# df_output['Threshold for Cumulative, vader_glove'] = list_threshold_cum_polarity_am_glove
df_output['Accuracy for Cumulative, am_glove'] = list_accuracy_cum_prediction_am_glove
# df_output['Threshold for Average, vader_glove'] = list_threshold_avg_polarity_am_glove
df_output['Accuracy for Average, am_glove'] = list_acccuray_avg_prediction_am_glove
# df_output['Threshold for Cumulative, vader_crawl'] = list_threshold_cum_polarity_am_crawl
df_output['Accuracy for Cumulative, am_crawl'] = list_accuracy_cum_prediction_am_crawl
# df_output['Threshold for Average, vader_crawl'] = list_threshold_avg_polarity_am_crawl
df_output['Accuracy for Average, am_crawl'] = list_acccuray_avg_prediction_am_crawl


# SAVE RESULTS
if WRITE_CSV:
    print('\nWriting into CSV')
    df_output.to_csv('../outputs/document_sentiment_accuracy.csv')

# PLOT THE DATA INTO A BAR-PLOT
if VISUAL:
    dict_modified = {'Domains': df_output['Domains'].tolist() + df_output['Domains'].to_list(),
                     'Embeddings': ['GloVe' for _ in range(24)] + ['fastText' for _ in range(24)],
                     'Accuracy': df_output['Accuracy for Average, am_glove'].tolist() +
                                 df_output['Accuracy for Average, am_crawl'].tolist()
                     }
    modified_df = pd.DataFrame(dict_modified)

    plt.figure(figsize=(10, 5))
    sns.set(style="whitegrid")
    chart = sns.barplot(x="Domains", y="Accuracy", hue="Embeddings", data=modified_df)

    chart.set_xticklabels(
        chart.get_xticklabels(),
        rotation=20,
        horizontalalignment='right',
        fontweight='light'
        # fontsize='x-small'
    )
    # chart.axes.axhline(avg_accuracy_vader*0.99, ls=':', color='black', linewidth=1.5)
    chart.axes.axhline(avg_accuracy_vader_glove, color='black', linewidth=1.5)
    chart.axes.axhline(avg_accuracy_vader_crawl, ls='--', color='black', linewidth=1.5)
    # chart.axes.text(0.6, 0.680, "VADER baseline with GloVe Embeddings", color='black', fontweight='bold')
    # chart.axes.text(0.5, 0.73568, "VADER baseline with fastText Embeddings", color='blue', fontweight='bold')
    plt.tight_layout()
    plt.show()